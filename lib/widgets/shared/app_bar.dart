import 'package:flutter/material.dart';
import 'package:test_app/helpers/colors.dart';

class AppBarWidget extends StatefulWidget {
  AppBarWidget({Key key}) : super(key: key);

  @override
  _AppBarWidgetState createState() => _AppBarWidgetState();
}

class _AppBarWidgetState extends State<AppBarWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'X',
          style: TextStyle(
              color: primary, fontWeight: FontWeight.bold, fontSize: 27),
        ),
        Text(
          'E',
          style: TextStyle(
              color: Colors.lightBlue[100],
              fontWeight: FontWeight.bold,
              fontSize: 27),
        )
      ],
    );
  }
}
