import 'package:flutter/material.dart';
import 'package:rating_bar/rating_bar.dart';
import 'package:test_app/pages/product_detail.dart';

class ProductList extends StatelessWidget {
  final int id;
  final Color color;
  final String discount;
  final bool isWishlist;
  final String name;
  final String price;
  final double rating;
  final String image;
  final Color backgroundColor;
  final String desc;

  ProductList(
      {this.id,
      this.color,
      this.discount,
      this.isWishlist,
      this.name,
      this.price,
      this.rating,
      this.image,
      this.desc,
      this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    Map detail = {
      'id': id,
      'color': color,
      'discount': discount,
      'isWishlist': isWishlist,
      'name': name,
      'price': price,
      'rating': rating,
      'image': image,
      'backgroundColor': backgroundColor,
      'desc': desc,
    };
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        // padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                discount != null
                    ? Container(
                        margin: EdgeInsets.only(left: 15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.blue[200],
                        ),
                        width: 35,
                        height: 20,
                        child: Center(
                          child: Text(
                            discount,
                            style: TextStyle(fontSize: 11),
                          ),
                        ),
                      )
                    : Container(),
                IconButton(
                    icon: Icon(Icons.favorite,
                        color: isWishlist ? Colors.red : Colors.grey),
                    onPressed: null),
              ],
            ),
            InkWell(
              onTap: () {
                Navigator.of(context)
                    .pushNamed(ProductDetail.routeName, arguments: detail);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: backgroundColor,
                    ),
                    width: 100,
                    height: 100,
                    child: Stack(
                      //alignment:new Alignment(x, y)
                      children: <Widget>[
                        Positioned(
                            left: 0,
                            bottom: 15,
                            child: Hero(
                              tag: 'detailProduct$id',
                              child: image != null
                                  ? Image.asset(
                                      image,
                                      width: 100,
                                    )
                                  : Text(''),
                            ))
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () => Navigator.of(context)
                  .pushNamed(ProductDetail.routeName, arguments: detail),
              child: Column(
                children: [
                  Text(
                    name,
                    style: TextStyle(
                      color: color,
                      // fontWeight: FontWeight.bold,
                      fontSize: 15,
                    ),
                  ),
                  Text(
                    price,
                    style: TextStyle(
                      color: color,
                      fontWeight: FontWeight.w900,
                      fontSize: 15,
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RatingBar.readOnly(
                  initialRating: rating,
                  isHalfAllowed: true,
                  halfFilledIcon: Icons.star_half,
                  filledIcon: Icons.star,
                  emptyIcon: Icons.star_border,
                  size: 15,
                  filledColor: Colors.yellow[600],
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  rating.toString(),
                  style: TextStyle(color: Colors.black54, fontSize: 12),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
