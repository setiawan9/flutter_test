import 'package:flutter/material.dart';
import 'package:test_app/helpers/colors.dart';
import 'package:test_app/widgets/shared/app_bar.dart';

class ProductDetail extends StatefulWidget {
  ProductDetail({Key key, this.detailProduct}) : super(key: key);
  static const routeName = '/product/detail';

  final Map detailProduct;

  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final detail = ModalRoute.of(context).settings.arguments as Map;
    print(detail);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
            onPressed: () => Navigator.of(context).pop()),
        backgroundColor: Colors.grey[100],
        elevation: 0,
        title: AppBarWidget(),
        centerTitle: true,
        actions: [
          detail['isWishlist']
              ? Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: FloatingActionButton(
                    mini: true,
                    onPressed: () {
                      // Add your onPressed code here!
                    },
                    child: Icon(Icons.favorite),
                    backgroundColor: Colors.red,
                  ),
                )
              : FlatButton(
                  color: Colors.grey,
                  child: Icon(Icons.favorite),
                  onPressed: null),
        ],
      ),
      backgroundColor: Colors.grey[100],
      body: Stack(
        children: [
          CustomScrollView(
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Column(
                      // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            detail['discount'] != null
                                ? Container(
                                    // margin: EdgeInsets.only(left: 15),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.blue[200],
                                    ),
                                    width: 35,
                                    height: 20,
                                    child: Center(
                                      child: Text(
                                        detail['discount'],
                                        style: TextStyle(fontSize: 11),
                                      ),
                                    ),
                                  )
                                : Container(),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        size.width * 0.25),
                                    color: detail['backgroundColor']
                                        .withOpacity(0.5),
                                  ),
                                  width: size.width * 0.5,
                                  height: size.width * 0.5,
                                  child: Stack(
                                    //alignment:new Alignment(x, y)
                                    children: <Widget>[
                                      Positioned(
                                          left: 10,
                                          bottom: size.width * 0.12,
                                          child: Hero(
                                            tag: 'detailProduct${detail['id']}',
                                            child: detail['image'] != null
                                                ? Image.asset(
                                                    detail['image'],
                                                    width: size.width * 0.45,
                                                  )
                                                : Text(''),
                                          ))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          height: 300,
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      detail['name'],
                                      style: TextStyle(
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold,
                                          color: primary),
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.star,
                                          color: Colors.yellow[600],
                                          size: 20,
                                        ),
                                        Text(detail['rating'].toString())
                                      ],
                                    )
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      detail['desc'],
                                      style: TextStyle(color: primary),
                                      textAlign: TextAlign.left,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Size:',
                                      style: TextStyle(color: Colors.black45),
                                      textAlign: TextAlign.left,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          height: 30,
                                          width: 50,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              color: Colors.blue[200]),
                                          child: Center(
                                            child: Text(
                                              'US 6',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Container(
                                          height: 30,
                                          width: 50,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            // color: Colors.blue[200],
                                          ),
                                          child: Center(
                                            child: Text(
                                              'US 7',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Container(
                                          height: 30,
                                          width: 50,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            // color: Colors.blue[200],
                                          ),
                                          child: Center(
                                            child: Text(
                                              'US 8',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Container(
                                          height: 30,
                                          width: 50,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            // color: Colors.blue[200],
                                          ),
                                          child: Center(
                                            child: Text(
                                              'US 9',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25))),
              height: 60,
              width: size.width,
              child: Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      detail['price'],
                      style: TextStyle(
                          color: primary,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                    FloatingActionButton.extended(
                      elevation: 0,
                      onPressed: () {
                        // Add your onPressed code here!
                      },
                      label: Text(
                        'Add To Cart',
                        style: TextStyle(
                            color: primary, fontWeight: FontWeight.bold),
                      ),
                      icon: Icon(
                        Icons.shopping_cart,
                        color: primary,
                      ),
                      backgroundColor: Colors.grey[100],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],

        // Positioned(
        //   bottom: 0,
        //   left: 0,
        //   child: Container(
        //     color: Colors.white,
        //     height: 60,
        //     width: size.width,
        //   ),
        // )
      ),
    );
  }
}
