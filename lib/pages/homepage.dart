import 'package:flutter/material.dart';
import 'package:test_app/helpers/colors.dart';
import 'package:test_app/widgets/list_category.dart';
import 'package:test_app/widgets/product_list.dart';
import 'package:test_app/widgets/shared/app_bar.dart';

import '../widgets/shared/bottom_nav.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        leading: Icon(
          Icons.grid_view,
          color: Colors.black87,
          size: 25,
        ),
        backgroundColor: Colors.grey[100],
        elevation: 0,
        title: AppBarWidget(),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.black87,
            ),
            iconSize: 25,
            onPressed: null,
          )
        ],
      ),
      backgroundColor: Colors.grey[100],
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 0, 8, 8),
            child: CustomScrollView(
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(6, 0, 6, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Our Product',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                            Row(
                              children: [
                                Text('Sort by'),
                                Icon(Icons.keyboard_arrow_down)
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      CategoryList(),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
                SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisSpacing: 5,
                    mainAxisSpacing: 5,
                    childAspectRatio: 0.62,
                    crossAxisCount: 2,
                  ),
                  delegate: SliverChildListDelegate(
                    [
                      ProductList(
                          id: 1,
                          color: primary,
                          discount: '30%',
                          isWishlist: false,
                          image: 'assets/images/shoes/airmax20.png',
                          name: 'Nike Air Max 20',
                          price: 'Rp. 240.000',
                          rating: 4.5,
                          backgroundColor: Colors.pink[100],
                          desc:
                              'Built For Natural motion, the Nike Flex Shoes'),
                      ProductList(
                          id: 3,
                          color: primary,
                          discount: null,
                          isWishlist: false,
                          image: 'assets/images/shoes/excee.png',
                          name: 'Air Max Motion 2',
                          price: 'Rp. 400.000',
                          rating: 5,
                          backgroundColor: Colors.green[100],
                          desc:
                              'Built For Natural motion, the Nike Flex Shoes'),
                      ProductList(
                          id: 2,
                          color: primary,
                          discount: '30%',
                          isWishlist: false,
                          image: 'assets/images/shoes/excee.png',
                          name: 'Nike Air Max 20',
                          price: 'Rp. 240.000',
                          rating: 4.5,
                          backgroundColor: Colors.blue[100],
                          desc:
                              'Built For Natural motion, the Nike Flex Shoes'),
                      ProductList(
                          id: 4,
                          color: primary,
                          discount: null,
                          isWishlist: false,
                          image: 'assets/images/shoes/airmax20.png',
                          name: 'Leather Sneackers',
                          price: 'Rp. 240.000',
                          rating: 4,
                          backgroundColor: Colors.yellowAccent[100],
                          desc:
                              'Built For Natural motion, the Nike Flex Shoes'),
                    ],
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      SizedBox(
                        height: 80,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Material(
              color: Colors.transparent,
              elevation: 70,
              child: Container(
                width: size.width,
                height: 60,
                // color: Colors.white,
                child: Stack(
                  children: [
                    CustomPaint(
                      size: Size(size.width, 80),
                      painter: BNBCustomPainter(),
                    ),
                    Center(
                      heightFactor: 0.6,
                      child: FloatingActionButton(
                        onPressed: () {},
                        backgroundColor: primary,
                        child: Icon(Icons.shopping_cart_rounded),
                        elevation: 15,
                      ),
                    ),
                    Container(
                      width: size.width,
                      height: 80,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.home_filled,
                              color: primary,
                            ),
                            onPressed: () {},
                          ),
                          IconButton(
                            icon: Icon(Icons.favorite),
                            onPressed: () {},
                            color: Colors.black45,
                          ),
                          Container(
                            width: size.width * 0.20,
                          ),
                          IconButton(
                            icon: Icon(Icons.event_note),
                            onPressed: () {},
                            color: Colors.black45,
                          ),
                          IconButton(
                            icon: Icon(Icons.account_circle_rounded),
                            onPressed: () {},
                            color: Colors.black45,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
